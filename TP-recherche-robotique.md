# Travail de recherche sur la robotique

- A partir de vos recherches bibliographiques et de l’internet, on vous demande de faire un état de l’art de la robotique.
- Son impactsur les avancées de la technologie
- Les domaines d’utilisations
- Ses avantages et ses inconvénientspour l’hommedans la société.
- Pour la suite, on s’intéresse à un type de robot bien connu dans lesmilieux scolaires, il s’agit du robot mBot.
  - Faites une étudecomplète de ce type de robotaussi bien physique que son fonctionnement logiciel
  - Pour vous aider à parfaire notre recherche sur le robotmBot, répondez aux questions suivantes en le détaillantsi nécessaire;
    - Quel est le but du robot mBot ?
    - Comment le robot mBot fonctionne ?
    - Quels sont les éléments du robot mBot ?
    - Qui a créé le robot mBot ?
    - Comment piloter mBot ?
    - Quel logiciel pour programmer mBot ?
    - Comment le robot se déplace ?
    - Comment est alimenté le robot mBot ?-Comment s'appelle le microcontrôleurdu mBot ?
    - Comment connecter le robot mBot à l'ordinateur ?
    - Comment faire la programmation d'un robot ?

---

# Response

État de l'art de la robotique :
La robotique est un domaine en constante évolution qui cherche à développer des machines capables de reproduire des actions humaines, de manière autonome ou en étant contrôlées par un humain. Les robots sont utilisés dans une variété de domaines, tels que la médecine, l'industrie, la recherche spatiale, l'agriculture, l'exploration sous-marine, la défense et bien d'autres encore. Les avancées récentes dans la robotique incluent des robots plus intelligents, plus autonomes, plus collaboratifs, plus adaptatifs et plus capables d'interagir avec les humains.

Impact de la robotique sur les avancées technologiques :
La robotique a eu un impact majeur sur les avancées technologiques dans de nombreux domaines. Par exemple, les robots industriels ont révolutionné la production en permettant une automatisation accrue des tâches répétitives, augmentant ainsi l'efficacité et la productivité. Les robots chirurgicaux ont permis des interventions chirurgicales moins invasives, réduisant les risques pour les patients et permettant une récupération plus rapide. Les robots autonomes sont de plus en plus utilisés pour la surveillance, l'exploration et la recherche dans des environnements dangereux ou difficiles d'accès.

Domaines d'utilisation de la robotique :
Les domaines d'utilisation de la robotique sont nombreux et en constante expansion. Les robots sont utilisés dans l'industrie pour la production automatisée, dans les soins de santé pour les interventions chirurgicales, la réadaptation et l'assistance aux personnes âgées ou handicapées, dans l'exploration spatiale et sous-marine, dans la défense pour la surveillance et la détection de menaces, dans l'agriculture pour la récolte et le traitement des cultures, dans les jeux vidéo et les divertissements pour l'interaction avec les joueurs, et bien plus encore.

Avantages et inconvénients de la robotique pour l'homme dans la société :
Les avantages de la robotique pour l'homme sont nombreux, notamment une plus grande efficacité, une réduction des erreurs, une amélioration de la sécurité et une capacité à accomplir des tâches qui seraient difficiles, voire impossibles, pour les humains. Cependant, il y a aussi des inconvénients, tels que la perte d'emplois pour les travailleurs qui sont remplacés par des robots, la dépendance à la technologie et l'impact potentiel sur la santé mentale et physique des humains.

Étude du robot mBot :
a) Physique et fonctionnement logiciel :
Le robot mBot est un robot éducatif conçu pour les bases de la programmation, de l'électronique et de la robotique. Il est équipé d'un microcontrôleur Arduino et peut être contrôlé grace au code insérer dedans.

b) Réponses aux questions sur le robot mBot :

Quel est le but du robot mBot ?
Le but du robot mBot est d'enseigner aux enfants les bases de la programmation, de l'électronique et de la robotique.

Comment le robot mBot fonctionne ?
Le robot est programmé pour suivre une série d'inscruction qui lui permet de répondre à une problématique dans un contexte donnée.

<!-- Le robot mBot fonctionne en utilisant un microcontrôleur Arduino et en étant programmé à l'aide d'un logiciel de programmation graphique. Il peut être contrôlé à l'aide d'une télécommande ou programmé pour suivre des instructions spécifiques. -->

Quels sont les éléments du robot mBot ?
Les éléments du robot mBot comprennent un microcontrôleur Arduino, un châssis, deux roues motrices, un capteur de suivi de ligne, un capteur de distance ultrasonique, une télécommande, des modules de LED, de buzzer et d'infrarouge.

Qui a créé le robot mBot ?
Le robot mBot a été créé par la société Makeblock.

Comment piloter mBot ?
Le robot mBot peut être piloté à l'aide d'une télécommande fournie avec le kit. Il peut également être programmé pour suivre des instructions spécifiques en utilisant un logiciel de programmation graphique.

Quel logiciel pour programmer mBot ?
Le logiciel recommandé pour programmer le robot mBot est mBlock, un logiciel de programmation graphique basé sur Scratch.

Comment le robot se déplace ?
Le robot mBot se déplace en utilisant deux roues motrices et peut être programmé pour se déplacer dans différentes directions et à différentes vitesses.

Comment est alimenté le robot mBot ?
Le robot mBot est alimenté par une batterie rechargeable au lithium-ion.

Comment s'appelle le microcontrôleur du mBot ?
Le microcontrôleur du robot mBot est un Arduino basé sur la puce ATmega328P.

Comment connecter le robot mBot à l'ordinateur ?
Le robot mBot peut être connecté à l'ordinateur à l'aide d'un câble USB fourni avec le kit.

Comment faire la programmation d'un robot ?
La programmation du robot mBot se fait à l'aide d'un logiciel de programmation graphique tel que mBlock, qui permet de créer des programmes en utilisant des blocs de code visuels plutôt que du code texte. Le programme créé est ensuite téléchargé sur le microcontrôleur du robot pour être exécuté.
