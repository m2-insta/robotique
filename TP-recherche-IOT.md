Projet de recherche : IoT : Objets Connectés
I – Partie recherche
A la suite d’une recherche fructueuse sur Internet :
⦁    Présenter le développement des objets connectés avec le vocabulaire approprié.
⦁    Comment anticiper les évolutions à venir, liées aux objets connectés, en identifiant les sources d’innovation par secteur d’activité.
⦁    Comment les objets connectés se communiquent entre eux.
⦁    Qu’est-ce qu’un espace connecté ? Donnez des exemples.
⦁    Expliquez comment un hôpital devient un espace connecté.
⦁    Les enjeux de l’Internet des objets : naming,
⦁    Les enjeux de l’Internet des objets : les espaces connectés.
⦁    Les acteurs de l’Internet des objets (conséquences économiques et géopolitiques)
⦁    Etudiez un cas de votre choix.
II - En se basant sur vos recherches sur Internet et autres :
⦁    Définissez qu’est-ce qu’un ESP ?
⦁    Faites une étude comparatives entre l’ESP32 et l’ESP8266.
⦁    Quels sont les composants utilisés pour une mise en place du fonctionnement d’un ESP.
