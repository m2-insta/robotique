#include <Arduino.h>
#include <MeMCore.h>

#define SERIAL_BAUDRATE 9600
#define SPEED  60 // Percentage (between 0 to 100)
#define DISTANCE 10

enum direction { STOP, FORWARD, BACKWARD, LEFT, RIGHT } move = STOP;

MeDCMotor motorL(M1);
MeDCMotor motorR(M2);
MeLineFollower lineFollower(PORT_1);
MeUltrasonicSensor ultrasonicSensor(PORT_2);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(SERIAL_BAUDRATE);
}

bool distanceWarning(double distanceCmLimit) {
  // Serial.println(ultrasonicSensor.distanceCm());
  if (ultrasonicSensor.distanceCm() < distanceCmLimit) {
    return true;
  } else {
    return false;
  }
}

long motorSpeed(long speed) {
  long s = map(speed, 0, 100, 0, 255);
   return s;
}

void moving() {
      // motorL.stop();
      // motorR.stop();
      long s = motorSpeed(SPEED);
  switch (lineFollower.readSensors()) {
    case S1_OUT_S2_OUT:
      Serial.println("Out of line");
      motorL.run(s);
      motorR.run(s);
      break;
    case S1_IN_S2_OUT:
      Serial.println("Left");
      motorL.run(-s/3);
      motorR.run(s);
      break;
    case S1_OUT_S2_IN:
      Serial.println("Right");
      motorL.run(-s);
      motorR.run(s/4);
      break;
    case S1_IN_S2_IN:
      Serial.println("On black line");
      motorL.run(-s);
      motorR.run(s);
      break;
    }
  }

void loop() {
  if (distanceWarning(DISTANCE)) {
    Serial.print("Detect collision [ ");
    Serial.print(ultrasonicSensor.distanceCm());
    Serial.print(" cm]");
    Serial.println("");
    motorL.stop();
    motorR.stop();
  } else {
    moving();    
  }
  delay(5);
}