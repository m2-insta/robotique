#ifndef I2C_H
#define I2C_H

#include <Arduino.h>
#include <Wire.h>

struct I2CDevice {
    char* address;
};

class I2C {
    public:
        I2CDevice* scan(void);
};

#endif