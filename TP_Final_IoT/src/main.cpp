#include <Arduino.h>

#include <SPI.h>
#include <Wire.h>  
#include <LiquidCrystal_I2C.h>
#include <Adafruit_BMP085.h>


#define SERIAL_BAUDRATE 9600

LiquidCrystal_I2C lcd(0x3F,20,4);
Adafruit_BMP085 bmp;

void setup() {
  Serial.begin(SERIAL_BAUDRATE);

  if (!bmp.begin()) {
  	Serial.println("Could not find a valid BMP085 sensor, check wiring!");
  }

  lcd.init();                      // initialize the lcd 
  // Print a message to the LCD.
  lcd.backlight();

}

void loop() {
    lcd.setCursor(0,0);
  lcd.print("temp: "+String(bmp.readTemperature()) + "°C");
    lcd.setCursor(0,1);
  lcd.print("press: "+String(bmp.readPressure()) + "Pa");
  delay(1000);
}